#include <stdio.h>
#include <ctype.h>
#include <strings.h>

/*
 *
 * $ ./list-pretty-print "(+ 1 2 (+ (* 3 (+ (* 2 4) (+ 3 5))) (+ (- 10 7) 6)))"
 * (+ 1 2 (+ (* 3 (+ (* 2 4) (+ 3 5))) (+ (- 10 7) 6))):
 * -------------------------------
 * (+ 1 2
 *    (+ (* 3
 *          (+ (* 2 4)
 *             (+ 3 5)))
 *       (+ (- 10 7)
 *          6)))
 * -------------------------------
 * $ ./list-pretty-print "(+ 1 2 (+ (* 3 (+ (* 2 4) (+ 3 5))) (+ (- 10 7) 6)))"
 * (+ 1 2 (+ (* 3 (+ (* 2 4) (+ 3 5))) (+ (- 10 7) 6))):
 * -------------------------------
 * (+ 1
 *    2
 *    (+ (* 3
 *          (+ (* 2
 *                4)
 *             (+ 3
 *                5)))
 *       (+ (- 10
 *             7)
 *          6)))
 * -------------------------------
 */

static void pretty_print(const char *s);

int main(int argc, char *argv[])
{
    for (int i = 1; i < argc; i++) {
        printf("%s:\n", argv[i]);
        printf("-------------------------------\n");
        pretty_print(argv[i]);
        printf("\n-------------------------------\n");
    }
}

const char *specials = "()+-*/";

static const char *skip_spaces(const char *s)
{
    while (*s != '\0' && isspace(*s)) s++;
    return s;
}

static const char *arg(const char *s)
{
    while (*s != '\0' && !isspace(*s) && !index(specials, *s)) putchar(*s++);
    return s;
}

static void newline(void)
{
    putchar('\n');
}

static void tab(void)
{
    printf("   ");
}

static void indent(int n)
{
    for (int i = 0; i < n; i++) tab();
}

static void pretty_print(const char *s)
{
    if (!s) return; 

    char prev = '\0';
    int tabs = 0;

    s = skip_spaces(s);
    if (*s != '(') return;

    while (*s != '\0') {
        char curr = *s;

        if (curr == '(') {
            if (prev == ')') {
                newline();
                indent(tabs);
            }
            tabs++;
            putchar(curr);
            s++;
        } else if (curr == ')') {
            tabs--;
            putchar(curr);
            s++;
        } else if (index(specials, curr)) {
            // Any other special char except for '(' and ')'.
            putchar(curr);
            putchar(' ');
            s++;
        } else {
            if (prev == ')') {
                newline();
                indent(tabs);
            }
            s = arg(s);
            // Simualate "arg" -> "(arg)" so that the next '('
            // goes to the new line.
            curr = ')';
        }

        prev = curr;
        s = skip_spaces(s);
    }
}
