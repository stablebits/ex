package main

import (
	"fmt"
	"os"
	_ "sort"
	"strconv"
)

func main() {
	if len(os.Args) < 2 {
		os.Exit(1)
	}

	amount, err := strconv.Atoi(os.Args[1])
	exitIfErr(err)

	var coins []int
	for _, s := range os.Args[2:] {
		coin, err := strconv.Atoi(s)
		exitIfErr(err)
		coins = append(coins, coin)
	}
	//	sort.Ints(coins)

	ways := make(map[int]int, amount+1)

	// coin = coins[1]
	// change(sum, coins[1..N]) = change(sum, coins[2..N]) + change(sum-coin, coins[1..N)
	//
	// This is the "placeholder" for accessing ways[sum-coin] (2nd part of the sum above)
	// for the case of sum==coin, i.e. 1 way to make "sum" with the coin alone.
	//
	ways[0] = 1

	for _, coin := range coins {
		for i := coin; i <= amount; i++ {
			ways[i] += ways[i-coin]
		}
	}

	fmt.Println("Number of way to make change:", ways[amount])
}

func exitIfErr(err error) {
	if err != nil {
		panic(err)
	}
}
